
include_recipe 'nginx'
include_recipe "nodejs::nodejs_from_binary"
#include_recipe "nodejs"

user = "ubuntu"
webapp_source = "/home/" + user + "/webapp_source"
webapp_bin_source = webapp_source + "/bin/"
document_root = "/home/" + user + "/webapp/"

git_ssh_key_path = '/home/ubuntu/.gitaccesskeys'

repo = node[:webapp_deploy][:app_repo]#"https://voonik5cell:Lunatick0der@github.com/Voonik/elite_portal.git"
#portal_repo = "https://voonik5cell:Lunatick0der@github.com/Voonik/vilara_wap.git"
if node['env'] == 'staging'
	branch = 'staging'
elsif node['env'] == 'production'
	branch = 'master'
end

directory webapp_source do
	owner user
	group user
	mode '0755'
	action :create
end


directory document_root do
	owner user
	group user
	mode '0755'
	action :create
end

# directory git_ssh_key_path do
# 	owner user
# 	group user
# 	mode '0755'
# 	action :create
# end

template "/home/#{user}/.ssh/id_rsa_temp" do
	source 'id_rsa.erb'
	owner user
	group user
	mode '0400'
end

template "/home/#{user}/.ssh/config" do
	source 'ssh_config.erb'
	owner user
	group user
	mode '0755'
end

# template git_ssh_key_path+'/id_rsa.pub' do
# 	source 'id_rsa.pub.erb'
# 	owner user
# 	group user
# 	mode '0400'
# end

# file git_ssh_key_path+"/git_wrapper.sh" do
#   owner user
#   mode "0755"
#   content "#!/bin/sh\nexec /usr/bin/ssh -i "+git_ssh_key_path+"/id_rsa \"$@\""
# end

execute 'Verifying host' do
	user user
	command 'ssh -oStrictHostKeyChecking=no -T git@github.com'
	action :run
	ignore_failure true
end


git webapp_source do
	repository repo
	revision branch
	user user
	action :sync	
	#ssh_wrapper git_ssh_key_path+"/git_wrapper.sh"
end


nodejs_npm "my_private_module" do
	path webapp_source
	json true
	user user
end

execute 'installing bower packages' do
	cwd webapp_source
	user user
	environment ({'HOME' => '/home/' + user, 'USER' => user})
	command 'bower install --force-latest'
	action :run
end

execute 'compile using grunt' do
	cwd webapp_source
	user user
	command 'grunt --force'
	action :run
end


execute 'copy compiled source to document root' do
	cwd webapp_source
	user user
	environment ({'HOME' => '/home/' + user, 'USER' => user})
	command 'cp -r ' + webapp_bin_source + '* ' + document_root
	action :run
end


execute "Deleting default nginx conf" do
	command 'rm /etc/nginx/sites-enabled/*default*'
end

template "/etc/nginx/sites-enabled/webapp" do
	source 'webapp.conf.erb'
	owner 'root'
	group 'root'
	mode '0755'
	notifies :reload, "service[nginx]", :immediate
end

template "/home/ubuntu/webapp/robots.txt" do
	source 'robots.txt.erb'
	owner 'root'
	group 'root'
	mode '0755'
end
