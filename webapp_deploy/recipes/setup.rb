include_recipe 'nodejs'

package 'nodejs' do
	action :install
end

nodejs_npm "grunt-cli"
nodejs_npm "karma"
nodejs_npm "bower"


