

user = 'ubuntu'
prerender_repo = "https://github.com/prerender/prerender.git"
prerender_source = '/home/ubuntu/prerender/'


git prerender_source do
	repository prerender_repo
	revision 'master'
	user user
	action :sync
end


nodejs_npm "my_private_module" do
	path prerender_source
	json true
	user user
end

execute 'Starting prerender server...' do
	cwd prerender_source
	user user
	environment ({'HOME' => '/home/' + user, 'USER' => user})
	command 'nohup node server.js &'
	action :run
end